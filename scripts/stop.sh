#!/usr/bin/env bash

[[ -e /etc/init/hello_world.conf ]] \
  && status hello_world | \
    grep -q '^hello_world start/running, process' \
  && [[ $? -eq 0 ]] \
  && stop hello_world || echo "Application not started"
